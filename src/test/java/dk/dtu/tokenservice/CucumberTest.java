package dk.dtu.tokenservice;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/dk/dtu/tokenservice/feature",
        monochrome = true,
        snippets = SnippetType.CAMELCASE,
        glue = "dk.dtu.tokenservice.step"
)
public class CucumberTest {
}
