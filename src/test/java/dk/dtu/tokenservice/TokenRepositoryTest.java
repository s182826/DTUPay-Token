package dk.dtu.tokenservice;

import dk.dtu.tokenservice.exception.TokenNotFoundException;
import dk.dtu.tokenservice.model.Token;
import dk.dtu.tokenservice.repository.TokenRepository;
import org.assertj.core.util.Sets;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 *  @author Janos Richard Pekk (s192617)
 */
public class TokenRepositoryTest {

    private static final String USER_ID = "1";
    private static final String NOT_EXISTING_TOKEN_ID = "2";
    private TokenRepository repository;

    @Before
    public void init() {
        this.repository = new TokenRepository();
    }

    @Test
    public void tokenCreateTest() {
        Token token = new Token();
        Set<Token> found = saveTokenAndReturn(Sets.newHashSet(Collections.singletonList(token)));

        assertEquals(1, found.size());
        assertTrue(found.contains(token));
    }

    @Test
    public void removeTokenFromUser() {
        Token token = new Token();
        Set<Token> found = saveTokenAndReturn(Sets.newHashSet(Collections.singletonList(token)));

        assertEquals(1, found.size());

        repository.removeTokenForUser(token.getUuid().toString(), USER_ID);

        Set<Token> foundAfterRemove = repository.getAllForUser(USER_ID);

        assertNotNull(foundAfterRemove);
        assertEquals(0, foundAfterRemove.size());
    }

    @Test(expected = TokenNotFoundException.class)
    public void tokenNotFoundTest() {
        repository.removeTokenForUser(NOT_EXISTING_TOKEN_ID, USER_ID);
    }

    private Set<Token> saveTokenAndReturn(Set<Token> tokensToSave) {
        repository.addTokensForUser(USER_ID, new HashSet<>(), tokensToSave);
        return repository.getAllForUser(USER_ID);
    }

}
