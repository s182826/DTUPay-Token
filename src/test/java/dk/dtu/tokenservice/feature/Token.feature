Feature: Token
  Description: Managing tokens

  Scenario: Generating new tokens for the user
    Given the system is initized
    And a user
    And an amount
    When generating the given amount of token for the given user
    Then the given amount of token is generated for the user

  Scenario: A user uses a token
    Given the system is initized
    Given a user with tokens
    When the given user uses a token
    Then the token gets used
