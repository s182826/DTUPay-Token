package dk.dtu.tokenservice.step;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dk.dtu.tokenservice.model.Token;
import dk.dtu.tokenservice.repository.TokenRepository;
import dk.dtu.tokenservice.service.TokenManagerService;

import java.util.Set;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *  @author Bence Burom (s192620)
 */
public class TokenStep {

    private TokenManagerService tokenService;

    private String userId;
    private Set<Token> tokens;
    private int rightAmount;
    private Token chosenToken;

    @Given("^the system is initized$")
    public void theSystemIsInitized() {
        tokenService = new TokenManagerService(new TokenRepository());
    }

    @Given("^a user$")
    public void a_user() {
        userId = UUID.randomUUID().toString();
    }

    @Given("^an amount$")
    public void an_amount() {
        rightAmount = 5;
    }

    @When("^generating the given amount of token for the given user$")
    public void generating_the_given_amount_of_token_for_the_given_user() {
        tokenService.generateTokensForUser(rightAmount, userId);
    }

    @Then("^the given amount of token is generated for the user$")
    public void the_given_amount_of_token_is_generated_for_the_user() {
        tokens = tokenService.getTokensForUser(userId);
        assertEquals(tokens.size(), rightAmount);
    }

    @Given("^a user with tokens$")
    public void a_user_with_tokens() {
        userId = UUID.randomUUID().toString();
        tokenService.generateTokensForUser(1, userId);
    }

    @When("^the given user uses a token$")
    public void the_given_user_uses_a_token() {
        tokens = tokenService.getTokensForUser(userId);
        chosenToken = tokens.iterator().next();
        tokenService.useToken(chosenToken.getUuid().toString(), userId);
    }

    @Then("^the token gets used$")
    public void the_token_gets_used() {
        tokens = tokenService.getTokensForUser(userId);
        assertThat(tokens, not(hasItem(chosenToken)));
    }
}
