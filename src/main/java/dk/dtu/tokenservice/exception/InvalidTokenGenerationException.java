package dk.dtu.tokenservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *  @author Daniel Szajcz (s192623)
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidTokenGenerationException extends RuntimeException {

    public InvalidTokenGenerationException(String message) {
        super(message);
    }

    public InvalidTokenGenerationException() {
        super("Maximum number of tokens for request is 5!");
    }

}
