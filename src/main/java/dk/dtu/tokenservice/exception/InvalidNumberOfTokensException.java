package dk.dtu.tokenservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *  @author Daniel Szajcz (s192623)
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidNumberOfTokensException extends RuntimeException {

    public InvalidNumberOfTokensException(int unusedTokens){
        super("Invalid number of tokens: " + unusedTokens);
    }

}
