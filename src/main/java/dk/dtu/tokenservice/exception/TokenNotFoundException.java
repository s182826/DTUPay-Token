package dk.dtu.tokenservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

/**
 *  @author Janos Richard Pekk (s192617)
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class TokenNotFoundException extends RuntimeException {

    public TokenNotFoundException(String id) {
        super ("Token not found with id " + id);
    }

}
