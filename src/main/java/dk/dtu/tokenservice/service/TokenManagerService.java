package dk.dtu.tokenservice.service;

import dk.dtu.tokenservice.exception.InvalidNumberOfTokensException;
import dk.dtu.tokenservice.exception.InvalidTokenException;
import dk.dtu.tokenservice.exception.InvalidTokenGenerationException;
import dk.dtu.tokenservice.message.MessageSender;
import dk.dtu.tokenservice.model.Token;
import dk.dtu.tokenservice.repository.TokenRepository;
import gherkin.deps.com.google.gson.JsonObject;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 *  @author Daniel Szajcz (s192623)
 */
@Service
@Log4j2
public class TokenManagerService {

    private final TokenRepository tokenRepository;

    public TokenManagerService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public Set<Token> getTokensForUser(String userId) {
        return tokenRepository.getAllForUser(userId);
    }

    public void generateTokensForUser(int number, String userId) {
        if (number > 5) {
            throw new InvalidTokenGenerationException();
        }
        Set<Token> tokens = new HashSet<>();
        for (int i = 0; i < number; i++) {
            Token newToken = new Token();
            tokens.add(newToken);
        }
        Set<Token> currentTokens = tokenRepository.getAllForUser(userId);
        if (currentTokens.size() > 1) {
            throw new InvalidNumberOfTokensException(currentTokens.size());
        }
        tokenRepository.addTokensForUser(userId, currentTokens, tokens);
    }

    public void useToken(String tokenId, String userId) {
        tokenRepository.removeTokenForUser(tokenId, userId);
    }

    public void addUserCprNumber(String cpr) {
        log.info("User registration message received. Registering cpr number " + cpr);
        tokenRepository.addUserByCprNumber(cpr);
    }

    public boolean validateToken(String token, String customerCpr, JsonObject payload) {
        log.info("Token validation requested for customer " + customerCpr);
        Set<Token> tokens = tokenRepository.getAllForUser(customerCpr);
        Token tokenToSearch = new Token(UUID.fromString(token));
        return tokens.contains(tokenToSearch);
    }
}
