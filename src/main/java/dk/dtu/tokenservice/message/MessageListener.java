package dk.dtu.tokenservice.message;

import dk.dtu.tokenservice.exception.InvalidTokenException;
import dk.dtu.tokenservice.service.TokenManagerService;
import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.JsonObject;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *  @author Bence Burom (s192620)
 */
@Service
@Log4j2
public class MessageListener {

    private final TokenManagerService service;
    private final Gson gson;
    private final MessageSender messageSender;

    @Autowired
    public MessageListener(TokenManagerService tokenManagerService, MessageSender messageSender) {
        this.service = tokenManagerService;
        this.gson = new Gson();
        this.messageSender = messageSender;
    }

    @RabbitListener(queues = "userCreatedToken")
    public void userCreatedMessage(final Message message) {
        String body = new String(message.getBody());
        JsonObject jsonObject = gson.fromJson(body, JsonObject.class);
        service.addUserCprNumber(jsonObject.get("cprNumber").getAsString());
    }

    @RabbitListener(queues = "validateToken")
    public void validateTokenMessage(final Message message) {
        String body = new String(message.getBody());
        JsonObject jsonObject = gson.fromJson(body, JsonObject.class);

        String tokenId = jsonObject.get("tokenId").getAsString();
        jsonObject.remove("tokenId");

        String customerCpr = jsonObject.get("debtorCprNumber").getAsString();

        boolean result = service.validateToken(tokenId, customerCpr, jsonObject);
        if (result) {
            log.info("Token validation is successful for user " + customerCpr);
            messageSender.sendMessage(jsonObject);
        } else {
            log.error("Invalid token requested for user " + customerCpr);
            throw new AmqpRejectAndDontRequeueException("The requested token is not valid!");
        }
    }

}
