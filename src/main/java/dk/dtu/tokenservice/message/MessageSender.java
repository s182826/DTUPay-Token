package dk.dtu.tokenservice.message;

import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.JsonObject;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

/**
 *  @author Bence Burom (s192620)
 */
@Service
@Log4j2
public class MessageSender {

    private final RabbitTemplate rabbitTemplate;

    String exchange_path="bank-payment-exchange";
    String routing_key="bank-payment-exchange-pay-routing-key";

    public MessageSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(JsonObject payload) {
        rabbitTemplate.convertAndSend(exchange_path, routing_key, payload.toString());
    }


}
