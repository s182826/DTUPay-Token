package dk.dtu.tokenservice.message;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  @author Janos Richard Pekk (s192617)
 */
@Configuration
public class TokenMQConfiguration {

    public static final String TOKEN_EXCHANGE_NAME = "token-user-exchange";
    public static final String TOKEN_ROUTING_KEY = "token-user-exchange-add-cpr-routing-key";
    public static final String QUEUE_USER_CREATED_NAME = "userCreatedToken";

    public static final String PAYMENT_EXCHANGE_NAME = "deployment-request";
    public static final String PAYMENT_ROUTING_KEY = "deployment-request-routing-key";
    public static final String QUEUE_VALIDATE_TOKEN = "validateToken";

    // BANK
    @Bean
    public TopicExchange appExchange() {
        return new TopicExchange(TOKEN_EXCHANGE_NAME);
    }

    @Bean
    public Queue transactionQueue() { return new Queue(QUEUE_USER_CREATED_NAME); }

    @Bean
    public Binding declareBindingTransaction() {
        return BindingBuilder.bind(transactionQueue()).to(appExchange()).with(TOKEN_ROUTING_KEY);
    }

    // PAYMENT
    @Bean
    public TopicExchange paymentExchange() {
        return new TopicExchange(PAYMENT_EXCHANGE_NAME);
    }

    @Bean
    public Queue paymentQueue() { return new Queue(QUEUE_VALIDATE_TOKEN); }

    @Bean
    public Binding declareBindingPayment() {
        return BindingBuilder.bind(paymentQueue()).to(paymentExchange()).with(PAYMENT_ROUTING_KEY);
    }

}
