package dk.dtu.tokenservice.controller;


import dk.dtu.tokenservice.model.Token;
import dk.dtu.tokenservice.service.TokenManagerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 *  @author Bence Burom (s192620)
 */
@RestController
@RequestMapping("/api/tokens")
public class TokenController {

    private final TokenManagerService tokenService;

    public TokenController(TokenManagerService tokenService) {
        this.tokenService = tokenService;
    }

    @PutMapping(path = "/{amount}/users/{userId}")
    @ResponseStatus(HttpStatus.CREATED)
    Set<Token> generateTokensForUser(@PathVariable int amount, @PathVariable String userId) {
        tokenService.generateTokensForUser(amount, userId);
        return tokenService.getTokensForUser(userId);
    }

    @PutMapping(path = "/{tokenId}/users/{userId}/use")
    void useToken(@PathVariable String tokenId, @PathVariable String userId) {
        tokenService.useToken(tokenId, userId);
    }
}
