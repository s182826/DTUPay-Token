package dk.dtu.tokenservice.repository;

import dk.dtu.tokenservice.exception.TokenNotFoundException;
import dk.dtu.tokenservice.model.Token;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *  @author Bence Burom (s192620)
 */
@Component
public class TokenRepository implements ITokenRepository {

    private Map<String, Set<Token>> tokens = new HashMap<>();

    @Override
    public Set<Token> getAllForUser(String userId) {
        Set<Token> tokensForUser = tokens.get(userId);
        if (tokensForUser == null) {
            return new HashSet<>();
        } else {
            return tokensForUser;
        }
    }

    @Override
    public void removeTokenForUser(String tokenId, String userId) {
        Set<Token> tokensForUser = getAllForUser(userId);

        Token tokenToRemove = tokensForUser.stream()
                .filter(token -> token.getUuid().toString().equals(tokenId))
                .findFirst().orElseThrow(() -> new TokenNotFoundException(tokenId));

        Set<Token> tokenSet = tokens.get(userId);
        tokenSet.remove(tokenToRemove);
        tokens.put(userId, tokenSet);
    }

    @Override
    public void addTokensForUser(String userId, Set<Token> currentTokens, Set<Token> tokensToAdd) {
        currentTokens.addAll(tokensToAdd);
        tokens.put(userId, currentTokens);
    }

    @Override
    public void addUserByCprNumber(String cpr) {
        if (tokens.containsKey(cpr)) return;
        tokens.put(cpr, new HashSet<>());
    }

}
