package dk.dtu.tokenservice.repository;


import dk.dtu.tokenservice.model.Token;

import java.util.Set;

/**
 *  @author Janos Richard Pekk (s192617)
 *  @author Daniel Szajcz (s192623)
 *  @author Bence Burom (s192620)
 */
public interface ITokenRepository {

    Set<Token> getAllForUser(String userId);
    void removeTokenForUser(String tokenId, String userId);
    void addTokensForUser(String userId, Set<Token> currentTokens, Set<Token> tokens);
    void addUserByCprNumber(String cpr);

}
