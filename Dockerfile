FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/token-service-0.0.1-SNAPSHOT.jar /usr/src
EXPOSE 8989
CMD java -jar token-service-0.0.1-SNAPSHOT.jar